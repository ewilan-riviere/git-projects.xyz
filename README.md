# git-projects.xyz

```bash
# https://github.com/jcalixte/vue-pwa-asset-generator
npx vue-pwa-asset-generator -a ./public/img/icon.png -o ./public/img/icons/
```

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
